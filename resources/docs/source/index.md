---
title: API Reference

language_tabs:
- javascript
- php
- bash

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#MainPage


<!-- START_77690a4251aa9a83e4ca16e28be916c4 -->
## getAppointments

Get a list of appointments by client and period.

> Example request:

```javascript
const url = new URL(
    "http://localhost/api/v1/appointments"
);

let params = {
    "id": "3",
    "date_from": "2020-06-14",
    "date_to": "2020-06-21",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/v1/appointments',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'id'=> '3',
            'date_from'=> '2020-06-14',
            'date_to'=> '2020-06-21',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```bash
curl -X GET \
    -G "http://localhost/api/v1/appointments?id=3&date_from=2020-06-14&date_to=2020-06-21" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "count": 11096,
    "items": [
        {
            "id": 6473,
            "book_datetime": "2020-06-03 00:18:04",
            "start_datetime": "2020-05-21 16:19:15",
            "end_datetime": "2020-06-20 11:18:13",
            "notes": "Sed qui vitae numquam expedita blanditiis rem tempora. Modi quibusdam quo ut molestiae. Aliquid error et distinctio est laudantium non non.",
            "hash": "dcbc18643d1b2f2a9827be1dfd680f44cb043a6f202809167fff68b4e3d3590a",
            "is_unavailable": 0,
            "id_users_provider": 3,
            "id_users_customer": 2,
            "id_services": 1,
            "id_google_calendar": ""
        },
        {
            "id": 17782,
            "book_datetime": "2020-06-12 09:45:11",
            "start_datetime": "2020-05-21 16:39:19",
            "end_datetime": "2020-06-16 12:36:06",
            "notes": "Ratione aspernatur est voluptates voluptas voluptas id fuga quam. Culpa et aperiam perferendis non et natus non. Ea tempora enim molestiae ut. Nulla totam maiores atque quasi quos.",
            "hash": "2dac1ca1a2c680df368e6aba6e07a05a60c8425fa8095eca46287978bbb27c17",
            "is_unavailable": 0,
            "id_users_provider": 3,
            "id_users_customer": 2,
            "id_services": 1,
            "id_google_calendar": ""
        },
        {
            "id": 14208,
            "book_datetime": "2020-05-28 15:15:23",
            "start_datetime": "2020-05-21 16:43:47",
            "end_datetime": "2020-06-20 13:35:30",
            "notes": "Ipsum expedita qui ut atque vel. Id ut sint aut. Veniam praesentium dolor qui commodi ipsum nesciunt minima ea. In qui qui qui hic odit.",
            "hash": "cd2c24f06c4caf48f33aa6f1f6a1b31c5ce721d944a4c5affee82a67155c6b33",
            "is_unavailable": 0,
            "id_users_provider": 3,
            "id_users_customer": 2,
            "id_services": 1,
            "id_google_calendar": ""
        }
    ]
}
```

### HTTP Request
`GET api/v1/appointments`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `id` |  optional  | string client ID.
    `date_from` |  optional  | string DateTime string of start period.
    `date_to` |  optional  | string DateTime string of end period.

<!-- END_77690a4251aa9a83e4ca16e28be916c4 -->

#general


<!-- START_73764e6fde425cf626e093259f36bf12 -->
## api/doc.json
> Example request:

```javascript
const url = new URL(
    "http://localhost/api/doc.json"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/api/doc.json',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```bash
curl -X GET \
    -G "http://localhost/api/doc.json" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "variables": [],
    "info": {
        "name": "Laravel API",
        "_postman_id": "5bf14da5-d2c2-42b2-ab32-5eb0b7a2b8a9",
        "description": "",
        "schema": "https:\/\/schema.getpostman.com\/json\/collection\/v2.0.0\/collection.json"
    },
    "item": [
        {
            "name": "MainPage",
            "description": "",
            "item": [
                {
                    "name": "getAppointments",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/v1\/appointments",
                            "query": [
                                {
                                    "key": "id",
                                    "value": "151651",
                                    "description": "string client ID.",
                                    "disabled": false
                                },
                                {
                                    "key": "date_from",
                                    "value": "",
                                    "description": "string DateTime string of start period.",
                                    "disabled": true
                                },
                                {
                                    "key": "date_to",
                                    "value": "",
                                    "description": "string DateTime string of end period.",
                                    "disabled": true
                                }
                            ]
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]"
                        },
                        "description": "Get a list of appointments by client and period.",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "general",
            "description": "",
            "item": [
                {
                    "name": "api\/doc.json",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/doc.json",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]"
                        },
                        "description": "",
                        "response": []
                    }
                }
            ]
        }
    ]
}
```

### HTTP Request
`GET api/doc.json`


<!-- END_73764e6fde425cf626e093259f36bf12 -->


