<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>API Reference</title>

    <link rel="stylesheet" href="{{ asset('/docs/css/style.css') }}" />
    <script src="{{ asset('/docs/js/all.js') }}"></script>


          <script>
        $(function() {
            setupLanguages(["javascript","php","bash"]);
        });
      </script>
      </head>

  <body class="">
    <a href="#" id="nav-button">
      <span>
        NAV
        <img src="/docs/images/navbar.png" />
      </span>
    </a>
    <div class="tocify-wrapper">
        <img src="/docs/images/logo.png" />
                    <div class="lang-selector">
                                  <a href="#" data-language-name="javascript">javascript</a>
                                  <a href="#" data-language-name="php">php</a>
                                  <a href="#" data-language-name="bash">bash</a>
                            </div>
                            <div class="search">
              <input type="text" class="search" id="input-search" placeholder="Search">
            </div>
            <ul class="search-results"></ul>
              <div id="toc">
      </div>
                    <ul class="toc-footer">
                                  <li><a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a></li>
                            </ul>
            </div>
    <div class="page-wrapper">
      <div class="dark-box"></div>
      <div class="content">
          <!-- START_INFO -->
<h1>Info</h1>
<p>Welcome to the generated API reference.
<a href="{{ route("apidoc.json") }}">Get Postman Collection</a></p>
<!-- END_INFO -->
<h1>MainPage</h1>
<!-- START_77690a4251aa9a83e4ca16e28be916c4 -->
<h2>getAppointments</h2>
<p>Get a list of appointments by client and period.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/appointments"
);

let params = {
    "id": "3",
    "date_from": "2020-06-14",
    "date_to": "2020-06-21",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;get(
    'http://localhost/api/v1/appointments',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
        ],
        'query' =&gt; [
            'id'=&gt; '3',
            'date_from'=&gt; '2020-06-14',
            'date_to'=&gt; '2020-06-21',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/v1/appointments?id=3&amp;date_from=2020-06-14&amp;date_to=2020-06-21" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "count": 11096,
    "items": [
        {
            "id": 6473,
            "book_datetime": "2020-06-03 00:18:04",
            "start_datetime": "2020-05-21 16:19:15",
            "end_datetime": "2020-06-20 11:18:13",
            "notes": "Sed qui vitae numquam expedita blanditiis rem tempora. Modi quibusdam quo ut molestiae. Aliquid error et distinctio est laudantium non non.",
            "hash": "dcbc18643d1b2f2a9827be1dfd680f44cb043a6f202809167fff68b4e3d3590a",
            "is_unavailable": 0,
            "id_users_provider": 3,
            "id_users_customer": 2,
            "id_services": 1,
            "id_google_calendar": ""
        },
        {
            "id": 17782,
            "book_datetime": "2020-06-12 09:45:11",
            "start_datetime": "2020-05-21 16:39:19",
            "end_datetime": "2020-06-16 12:36:06",
            "notes": "Ratione aspernatur est voluptates voluptas voluptas id fuga quam. Culpa et aperiam perferendis non et natus non. Ea tempora enim molestiae ut. Nulla totam maiores atque quasi quos.",
            "hash": "2dac1ca1a2c680df368e6aba6e07a05a60c8425fa8095eca46287978bbb27c17",
            "is_unavailable": 0,
            "id_users_provider": 3,
            "id_users_customer": 2,
            "id_services": 1,
            "id_google_calendar": ""
        },
        {
            "id": 14208,
            "book_datetime": "2020-05-28 15:15:23",
            "start_datetime": "2020-05-21 16:43:47",
            "end_datetime": "2020-06-20 13:35:30",
            "notes": "Ipsum expedita qui ut atque vel. Id ut sint aut. Veniam praesentium dolor qui commodi ipsum nesciunt minima ea. In qui qui qui hic odit.",
            "hash": "cd2c24f06c4caf48f33aa6f1f6a1b31c5ce721d944a4c5affee82a67155c6b33",
            "is_unavailable": 0,
            "id_users_provider": 3,
            "id_users_customer": 2,
            "id_services": 1,
            "id_google_calendar": ""
        }
    ]
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/v1/appointments</code></p>
<h4>Query Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>string client ID.</td>
</tr>
<tr>
<td><code>date_from</code></td>
<td>optional</td>
<td>string DateTime string of start period.</td>
</tr>
<tr>
<td><code>date_to</code></td>
<td>optional</td>
<td>string DateTime string of end period.</td>
</tr>
</tbody>
</table>
<!-- END_77690a4251aa9a83e4ca16e28be916c4 -->
<h1>general</h1>
<!-- START_73764e6fde425cf626e093259f36bf12 -->
<h2>api/doc.json</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/doc.json"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;get(
    'http://localhost/api/doc.json',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/doc.json" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "variables": [],
    "info": {
        "name": "Laravel API",
        "_postman_id": "5bf14da5-d2c2-42b2-ab32-5eb0b7a2b8a9",
        "description": "",
        "schema": "https:\/\/schema.getpostman.com\/json\/collection\/v2.0.0\/collection.json"
    },
    "item": [
        {
            "name": "MainPage",
            "description": "",
            "item": [
                {
                    "name": "getAppointments",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/v1\/appointments",
                            "query": [
                                {
                                    "key": "id",
                                    "value": "151651",
                                    "description": "string client ID.",
                                    "disabled": false
                                },
                                {
                                    "key": "date_from",
                                    "value": "",
                                    "description": "string DateTime string of start period.",
                                    "disabled": true
                                },
                                {
                                    "key": "date_to",
                                    "value": "",
                                    "description": "string DateTime string of end period.",
                                    "disabled": true
                                }
                            ]
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]"
                        },
                        "description": "Get a list of appointments by client and period.",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "general",
            "description": "",
            "item": [
                {
                    "name": "api\/doc.json",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/doc.json",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]"
                        },
                        "description": "",
                        "response": []
                    }
                }
            ]
        }
    ]
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/doc.json</code></p>
<!-- END_73764e6fde425cf626e093259f36bf12 -->
      </div>
      <div class="dark-box">
                        <div class="lang-selector">
                                    <a href="#" data-language-name="javascript">javascript</a>
                                    <a href="#" data-language-name="php">php</a>
                                    <a href="#" data-language-name="bash">bash</a>
                              </div>
                </div>
    </div>
  </body>
</html>