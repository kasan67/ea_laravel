<?php


namespace App\Domain;


use App\Models\Appointments;

class AppointmentsDomain
{

    /**
     * @param string $userId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function getAppointments(string $userId, string $dateFrom, string $dateTo): array
    {

        $items = Appointments::where([
            'is_unavailable' => false
        ])
            ->where('id_users_provider', $userId)
            ->orWhere('id_users_customer', $userId)
            ->whereBetween('start_datetime', [$dateFrom, $dateTo])
            ->orderBy('start_datetime', 'asc')
            ->get();

        $count = $items->count();

        return ['count' => $count, 'items' => $items->toArray()];
    }

}
