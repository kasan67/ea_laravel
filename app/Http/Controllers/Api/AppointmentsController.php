<?php

namespace App\Http\Controllers\Api;

use App\Domain\AppointmentsDomain;
use App\Http\Requests\GetAppointments;
use App\Http\Controllers\Controller;

class AppointmentsController extends Controller
{
    private $appointmentsDomain;

    public function __construct(AppointmentsDomain $appointmentsDomain)
    {
        $this->appointmentsDomain = $appointmentsDomain;
    }

    /**
     * getAppointments
     *
     * Get a list of appointments by client and period.
     *
     * @group MainPage
     * @queryParam id string client ID. Example: 3
     * @queryParam date_from string DateTime string of start period. Example: 2020-06-14
     * @queryParam date_to string DateTime string of end period. Example: 2020-06-21
     *
     * @response {"count": 11096,"items": [{"id": 6473,"book_datetime": "2020-06-03 00:18:04","start_datetime": "2020-05-21 16:19:15","end_datetime": "2020-06-20 11:18:13","notes": "Sed qui vitae numquam expedita blanditiis rem tempora. Modi quibusdam quo ut molestiae. Aliquid error et distinctio est laudantium non non.","hash": "dcbc18643d1b2f2a9827be1dfd680f44cb043a6f202809167fff68b4e3d3590a","is_unavailable": 0,"id_users_provider": 3,"id_users_customer": 2,"id_services": 1,"id_google_calendar": ""},{"id": 17782,"book_datetime": "2020-06-12 09:45:11","start_datetime": "2020-05-21 16:39:19","end_datetime": "2020-06-16 12:36:06","notes": "Ratione aspernatur est voluptates voluptas voluptas id fuga quam. Culpa et aperiam perferendis non et natus non. Ea tempora enim molestiae ut. Nulla totam maiores atque quasi quos.","hash": "2dac1ca1a2c680df368e6aba6e07a05a60c8425fa8095eca46287978bbb27c17","is_unavailable": 0,"id_users_provider": 3,"id_users_customer": 2,"id_services": 1,"id_google_calendar": ""},{"id": 14208,"book_datetime": "2020-05-28 15:15:23","start_datetime": "2020-05-21 16:43:47","end_datetime": "2020-06-20 13:35:30","notes": "Ipsum expedita qui ut atque vel. Id ut sint aut. Veniam praesentium dolor qui commodi ipsum nesciunt minima ea. In qui qui qui hic odit.","hash": "cd2c24f06c4caf48f33aa6f1f6a1b31c5ce721d944a4c5affee82a67155c6b33","is_unavailable": 0,"id_users_provider": 3,"id_users_customer": 2,"id_services": 1,"id_google_calendar": ""}]}
     * @param GetAppointments $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAppointments(GetAppointments $request)
    {
        $result = $this->appointmentsDomain->getAppointments(
            $request->get('id'),
            $request->get('date_from'),
            $request->get('date_to')
        );

        return response()->json($result);
    }
}
