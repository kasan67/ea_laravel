<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $created
 * @property string $modified
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $ip
 * @property string $type
 */
class Consents extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ea_consents';

    /**
     * @var array
     */
    protected $fillable = ['created', 'modified', 'first_name', 'last_name', 'email', 'ip', 'type'];

}
