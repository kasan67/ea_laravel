<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Users;

/**
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property boolean $is_admin
 * @property int $appointments
 * @property int $customers
 * @property int $services
 * @property int $users
 * @property int $system_settings
 * @property int $user_settings
 * @property Users[] $Users
 */
class Roles extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ea_roles';

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'is_admin', 'appointments', 'customers', 'services', 'users', 'system_settings', 'user_settings'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Users()
    {
        return $this->hasMany(Users::class, 'id_roles');
    }
}
