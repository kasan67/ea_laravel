<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_roles
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $mobile_number
 * @property string $phone_number
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zip_code
 * @property string $notes
 * @property Roles $Role
 * @property Appointments[] $AppointmentsCustomer
 * @property Appointments[] $AppointmentsProvider
 * @property Users[] $UserProviders
 * @property Services[] $Services
 * @property UserSettings $UserSettings
 */
class Users extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ea_users';

    /**
     * @var array
     */
    protected $fillable = ['id_roles', 'first_name', 'last_name', 'email', 'mobile_number', 'phone_number', 'address', 'city', 'state', 'zip_code', 'notes'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Role()
    {
        return $this->belongsTo(Roles::class, 'id_roles');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function AppointmentsCustomer()
    {
        return $this->hasMany(Appointments::class, 'id_users_customer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function AppointmentsProvider()
    {
        return $this->hasMany(Appointments::class, 'id_users_provider');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function UserProviders()
    {
        return $this->belongsToMany(Users::class, 'ea_secretaries_providers', 'id_users_secretary', 'id_users_provider');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Services()
    {
        return $this->belongsToMany(Services::class, 'ea_services_providers', 'id_users', 'id_services');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function UserSettings()
    {
        return $this->hasOne(UserSettings::class, 'id_users');
    }
}
