<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_users_secretary
 * @property int $id_users_provider
 * @property Users $UserSecretary
 * @property Users $UserProvider
 */
class Secretaries extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ea_secretaries_providers';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserSecretary()
    {
        return $this->belongsTo(Users::class, 'id_users_secretary');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserProvider()
    {
        return $this->belongsTo(Users::class, 'id_users_provider');
    }
}
