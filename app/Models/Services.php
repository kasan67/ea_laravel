<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_service_categories
 * @property string $name
 * @property int $duration
 * @property float $price
 * @property string $currency
 * @property string $description
 * @property string $availabilities_type
 * @property int $attendants_number
 * @property ServiceCategories $ServiceCategories
 * @property Appointments[] $Appointments
 * @property Users[] $Users
 */
class Services extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ea_services';

    /**
     * @var array
     */
    protected $fillable = ['id_service_categories', 'name', 'duration', 'price', 'currency', 'description', 'availabilities_type', 'attendants_number'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ServiceCategories()
    {
        return $this->belongsTo('App\Models\ServiceCategories', 'id_service_categories');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Appointments()
    {
        return $this->hasMany(Appointments::class, 'id_services');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Users()
    {
        return $this->belongsToMany(Users::class, 'ea_services_providers', 'id_services', 'id_users');
    }
}
