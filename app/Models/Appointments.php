<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_users_provider
 * @property int $id_users_customer
 * @property int $id_services
 * @property string $book_datetime
 * @property string $start_datetime
 * @property string $end_datetime
 * @property string $notes
 * @property string $hash
 * @property boolean $is_unavailable
 * @property string $id_google_calendar
 * @property Users $UserCustomer
 * @property Services $Services
 * @property Users $UserProvider
 */
class Appointments extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ea_appointments';

    /**
     * @var array
     */
    protected $fillable = ['id_users_provider', 'id_users_customer', 'id_services', 'book_datetime', 'start_datetime', 'end_datetime', 'notes', 'hash', 'is_unavailable', 'id_google_calendar'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserCustomer()
    {
        return $this->belongsTo(Users::class, 'id_users_customer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Services()
    {
        return $this->belongsTo(Services::class, 'id_services');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function UserProvider()
    {
        return $this->belongsTo(Users::class, 'id_users_provider');
    }
}
