<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_users
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property string $working_plan
 * @property boolean $notifications
 * @property boolean $google_sync
 * @property string $google_token
 * @property string $google_calendar
 * @property int $sync_past_days
 * @property int $sync_future_days
 * @property string $calendar_view
 * @property Users $Users
 */
class UserSettings extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ea_user_settings';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_users';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['username', 'password', 'salt', 'working_plan', 'notifications', 'google_sync', 'google_token', 'google_calendar', 'sync_past_days', 'sync_future_days', 'calendar_view'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Users()
    {
        return $this->belongsTo(Users::class, 'id_users');
    }
}
