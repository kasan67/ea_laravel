<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Appointments::class, static function (Faker $faker) {
    return [
        'id_users_provider' => '4',
        'id_users_customer' => '5',
        'id_services' => 1,
        'book_datetime' => $faker->dateTimeBetween('-1 month'),
        'start_datetime' => $faker->dateTimeBetween('-1 month'),
        'end_datetime' => $faker->dateTimeBetween('-1 week'),
        'notes' => $faker->text,
        'hash' => $faker->unique()->sha256,
        'is_unavailable' => $faker->boolean(90),
        'id_google_calendar' => '',
    ];
});
