<?php

use Illuminate\Database\Seeder;


class AppointmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Appointments::class, 10000)->create();
    }
}
