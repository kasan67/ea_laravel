<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEaSecretariesProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ea_secretaries_providers', function(Blueprint $table)
		{
			$table->integer('id_users_secretary')->index('id_users_secretary');
			$table->integer('id_users_provider')->index('id_users_provider');
			$table->primary(['id_users_secretary','id_users_provider']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ea_secretaries_providers');
	}

}
