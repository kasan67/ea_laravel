<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEaRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ea_roles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 256)->nullable();
			$table->string('slug', 256)->nullable();
			$table->boolean('is_admin')->nullable();
			$table->integer('appointments')->nullable();
			$table->integer('customers')->nullable();
			$table->integer('services')->nullable();
			$table->integer('users')->nullable();
			$table->integer('system_settings')->nullable();
			$table->integer('user_settings')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ea_roles');
	}

}
