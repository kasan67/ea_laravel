<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEaUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ea_users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('first_name', 256)->nullable();
			$table->string('last_name', 512)->nullable();
			$table->string('email', 512)->nullable();
			$table->string('mobile_number', 128)->nullable();
			$table->string('phone_number', 128)->nullable();
			$table->string('address', 256)->nullable();
			$table->string('city', 256)->nullable();
			$table->string('state', 128)->nullable();
			$table->string('zip_code', 64)->nullable();
			$table->text('notes', 65535)->nullable();
			$table->integer('id_roles')->index('id_roles');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ea_users');
	}

}
