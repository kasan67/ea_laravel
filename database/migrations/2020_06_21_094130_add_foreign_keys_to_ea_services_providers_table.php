<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEaServicesProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ea_services_providers', function(Blueprint $table)
		{
			$table->foreign('id_users', 'services_providers_users_provider')->references('id')->on('ea_users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_services', 'services_providers_services')->references('id')->on('ea_services')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ea_services_providers', function(Blueprint $table)
		{
			$table->dropForeign('services_providers_users_provider');
			$table->dropForeign('services_providers_services');
		});
	}

}
