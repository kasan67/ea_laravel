<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEaServicesProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ea_services_providers', function(Blueprint $table)
		{
			$table->integer('id_users');
			$table->integer('id_services')->index('id_services');
			$table->primary(['id_users','id_services']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ea_services_providers');
	}

}
