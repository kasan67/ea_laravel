<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEaAppointmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ea_appointments', function(Blueprint $table)
		{
			$table->foreign('id_users_customer', 'appointments_users_customer')->references('id')->on('ea_users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_services', 'appointments_services')->references('id')->on('ea_services')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_users_provider', 'appointments_users_provider')->references('id')->on('ea_users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ea_appointments', function(Blueprint $table)
		{
			$table->dropForeign('appointments_users_customer');
			$table->dropForeign('appointments_services');
			$table->dropForeign('appointments_users_provider');
		});
	}

}
