<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEaUserSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ea_user_settings', function(Blueprint $table)
		{
			$table->integer('id_users')->primary();
			$table->string('username', 256)->nullable();
			$table->string('password', 512)->nullable();
			$table->string('salt', 512)->nullable();
			$table->text('working_plan', 65535)->nullable();
			$table->boolean('notifications')->nullable()->default(0);
			$table->boolean('google_sync')->nullable()->default(0);
			$table->text('google_token', 65535)->nullable();
			$table->string('google_calendar', 128)->nullable();
			$table->integer('sync_past_days')->nullable()->default(5);
			$table->integer('sync_future_days')->nullable()->default(5);
			$table->string('calendar_view', 32)->nullable()->default('default');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ea_user_settings');
	}

}
