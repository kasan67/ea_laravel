<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEaSecretariesProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ea_secretaries_providers', function(Blueprint $table)
		{
			$table->foreign('id_users_secretary', 'secretaries_users_secretary')->references('id')->on('ea_users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_users_provider', 'secretaries_users_provider')->references('id')->on('ea_users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ea_secretaries_providers', function(Blueprint $table)
		{
			$table->dropForeign('secretaries_users_secretary');
			$table->dropForeign('secretaries_users_provider');
		});
	}

}
