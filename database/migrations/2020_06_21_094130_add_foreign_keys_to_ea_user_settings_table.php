<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEaUserSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ea_user_settings', function(Blueprint $table)
		{
			$table->foreign('id_users', 'user_settings_users')->references('id')->on('ea_users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ea_user_settings', function(Blueprint $table)
		{
			$table->dropForeign('user_settings_users');
		});
	}

}
