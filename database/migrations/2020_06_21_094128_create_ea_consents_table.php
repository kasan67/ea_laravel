<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEaConsentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ea_consents', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('created')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('modified')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('first_name', 256)->nullable();
			$table->string('last_name', 256)->nullable();
			$table->string('email', 512)->nullable();
			$table->string('ip', 256)->nullable();
			$table->string('type', 256)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ea_consents');
	}

}
