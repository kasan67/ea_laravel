<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEaServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ea_services', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 256)->nullable();
			$table->integer('duration')->nullable();
			$table->decimal('price', 10)->nullable();
			$table->string('currency', 32)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('availabilities_type', 32)->nullable()->default('flexible');
			$table->integer('attendants_number')->nullable()->default(1);
			$table->integer('id_service_categories')->nullable()->index('id_service_categories');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ea_services');
	}

}
