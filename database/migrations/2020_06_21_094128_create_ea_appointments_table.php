<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEaAppointmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ea_appointments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->dateTime('book_datetime')->nullable();
			$table->dateTime('start_datetime')->nullable();
			$table->dateTime('end_datetime')->nullable();
			$table->text('notes', 65535)->nullable();
			$table->text('hash', 65535)->nullable();
			$table->boolean('is_unavailable')->nullable()->default(0);
			$table->integer('id_users_provider')->nullable()->index('id_users_provider');
			$table->integer('id_users_customer')->nullable()->index('id_users_customer');
			$table->integer('id_services')->nullable()->index('id_services');
			$table->text('id_google_calendar', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ea_appointments');
	}

}
