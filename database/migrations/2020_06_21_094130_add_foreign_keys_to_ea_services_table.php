<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEaServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ea_services', function(Blueprint $table)
		{
			$table->foreign('id_service_categories', 'services_service_categories')->references('id')->on('ea_service_categories')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ea_services', function(Blueprint $table)
		{
			$table->dropForeign('services_service_categories');
		});
	}

}
